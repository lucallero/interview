
let routes = new Map()

function registerRoute (path, resource) {
  routes.set(path, resource)
}

function callRoute (path) {
  for (const key of routes.keys()) {
    if (path.startsWith(key)) {
      routes.get(key)(path.substring(key.length))
    }
  }
  throw new Error(`'${path}' not found`)
}

let A = (userId) => {
  console.log(userId)
}

let B = (path) => {
  console.log('B')
}

registerRoute('/rest/user/', A)
registerRoute('/rest/payments/', B)

callRoute('/rest/user/123') // calls A
callRoute('/rest/payments')
callRoute('/123')
