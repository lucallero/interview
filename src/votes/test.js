'use strict'
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const chai = require('chai')
const expect = require('chai').expect
const should = require('chai').should()
const deepEqualInAnyOrder = require('deep-equal-in-any-order')
const test = require('./index')
chai.use(deepEqualInAnyOrder)

describe('Found the most frequent item in an Array.', () => {
  let votes = [
    'Wild Bill',
    'Joana Dark',
    'Wild Bill',
    'Jerry Lewes',
    'Wild Bill',
    'Wild Bill',
    'Jerry Lewes',
    'Jerry Lewes',
    'Jerry Lewes',
    'Jerry Lewes',
    'Wild Bill'
  ]
  it('Should return 1 ', async () => {
    let result = test.main(votes)
    console.log(result)
    result.name.should.equal('Jerry Lewes')
  })
})
