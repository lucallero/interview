'use strict'
function main (listOfVotes) {
  let votingCount = new Map()

  for (let i = 0; i < listOfVotes.length; i++) {
    let candidate = listOfVotes[i]
    if (!votingCount.has(candidate)) {
      votingCount.set(candidate, { name: candidate, votes: 1, step: i })
    } else {
      votingCount.set(candidate, { name: candidate, votes: votingCount.get(candidate).votes + 1, step: i })
    }
  }

  let votingCountArray = Array.from(votingCount.values())

  votingCountArray.sort((a, b) => {
    if (a.votes < b.votes) return 1
    else if (a.votes > b.votes) return -1
    else if (a.votes === b.votes) {
      if (a.step < b.step) {
        return -1
      } else {
        return 1
      }
    }
  })

  console.log(votingCountArray)

  return votingCountArray[0]
}

module.exports = { main }
