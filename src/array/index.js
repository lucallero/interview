'use strict'
function main (array) {
  const occurrencies = new Map()
  for (const number of array) {
    if (occurrencies.has(number)) {
      occurrencies.set(number, (occurrencies.get(number) + 1))
    } else {
      occurrencies.set(number, 1)
    }
  }
  let bigger = { key: 0, value: 0 }
  for (const number of occurrencies.keys()) {
    if (occurrencies.get(number) > bigger.value) {
      bigger = { key: number, value: occurrencies.get(number) }
    }
  }
  return bigger.key
}
module.exports = { main }

let undefinedVar

console.log(undefinedVar + 1)
