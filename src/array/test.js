'use strict'
/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
const chai = require('chai')
const expect = require('chai').expect
const should = require('chai').should()
const deepEqualInAnyOrder = require('deep-equal-in-any-order')
const test = require('./index')
chai.use(deepEqualInAnyOrder)

describe('Found the most frequent item in an Array.', () => {
  it('Should return 1 ', async () => {
    let result = test.main([1, 3, 1, 3, 2, 1])
    result.should.equal(1)
  })

  it('Should return 3 ', async () => {
    let result = test.main([2, 3, 3, 3, 2, 1])
    result.should.equal(3)
  })
})
